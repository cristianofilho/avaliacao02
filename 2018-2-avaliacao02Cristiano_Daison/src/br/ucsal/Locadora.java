package br.ucsal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Locadora {
	private static List<Veiculo> lista = new ArrayList<>();

	public static void CadastrarOnibus(String placa, String modelo, Integer Ano_fabric, Double valor,
			Integer passageiros) {

		try {
			Onibus onibus = new Onibus(placa, modelo, Ano_fabric, valor, passageiros);
			lista.add(onibus);

		} catch (ValorNaoValidoException e) {

			e.printStackTrace();
		}

	}

	public static void CadastrarCaminhao(String placa, String modelo, Integer Ano_fabric, Double valor, Integer eixo,
			Integer carga) {

		try {
			Caminhao caminhao1 = new Caminhao(placa, modelo, Ano_fabric, valor, carga, eixo);
			lista.add(caminhao1);
		} catch (ValorNaoValidoException e) {
			e.printStackTrace();
		}
	}

	public static void ListarVeiculosOrdenadoPorValor() {
		Collections.sort(lista, new Comparator<Veiculo>() {

			@Override
			public int compare(Veiculo v1, Veiculo v2) {

				return Double.compare(v1.getValor(), v2.getValor());
			}

		});

		for (Veiculo veiculo : lista) {
			System.out.println(veiculo.getPlaca() + " " + veiculo.getValor() + " " + veiculo.CalcularCustoLocacao());
		}

	}

	public static void ListarVeiculosOrdenadoPorPlaca() {
		Collections.sort(lista);
		for (Veiculo veiculo : lista) {

			System.out.println(veiculo.toString());

		}

	}

	public static void ListarModelos() {
		Set<String> modelos = new HashSet<String>();
		for (Veiculo veiculo : lista) {
			modelos.add(veiculo.getModelo()); 
			

		}
		System.out.println(modelos);
	}
}
